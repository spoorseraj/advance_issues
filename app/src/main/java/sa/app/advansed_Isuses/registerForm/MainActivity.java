package sa.app.advansed_Isuses.registerForm;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.PermissionRequest;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import sa.app.advansed_Isuses.R;
import sa.app.advansed_Isuses.custom_views.MyEditText;
import sa.app.advansed_Isuses.utiles.BaseApplication;

public class MainActivity extends AppCompatActivity {
    CircleImageView img;
    MyEditText name, mobile, email;
    Uri uri = Uri.parse("android.resource://sa.app.advansed_Isuses/drawable/profile1");
    String url = uri.toString();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bind();
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("choice from camera");
        arrayAdapter.add("choice from gallery");
        ((ImageButton) findViewById(R.id.camera)).setOnClickListener(v -> {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setIcon(R.mipmap.ic_launcher);
            alertDialog.setTitle("Select One:");
            alertDialog.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
//                    Toast.makeText(MainActivity.this, arrayAdapter.getItem(i) + " " + i, Toast.LENGTH_SHORT).show();
                    if (i == 0) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, 100);
                    }
                    if (i == 1) {
                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, 200);
                    }

                }
            });
            alertDialog.show();
        });
        findViewById(R.id.save).setOnClickListener(v -> {
            save();
        });
        getPermission();
    }

    private void bind() {
        img = findViewById(R.id.img);
        name = findViewById(R.id.name);
        mobile = findViewById(R.id.mobile);
        email = findViewById(R.id.email);

    }

    private void save() {
        RegisterModel rm = new RegisterModel();
        rm.setName(name.get());
        rm.setEmail(email.get());
        rm.setMobile(mobile.get());
        rm.setUrlImage(url);
        rm.save();
        Toast.makeText(this, "رکورد جدید اضافه شد", Toast.LENGTH_SHORT).show();
        finish();
    }

    private void getPermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE

                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {


            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<com.karumi.dexter.listener.PermissionRequest> permissions, PermissionToken token) {

            }
        }).check();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
//            Toast.makeText(this, "ok", Toast.LENGTH_SHORT).show();
            if (requestCode == 100) {
                url = data.getData().toString();
                Glide.with(this).load(data.getData()).into(img);

            } else if (requestCode == 200) {
                url = data.getData().toString();
                Glide.with(this).load(data.getData()).into(img);
            }
        }
    }
}
