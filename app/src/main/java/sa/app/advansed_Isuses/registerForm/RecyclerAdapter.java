package sa.app.advansed_Isuses.registerForm;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import sa.app.advansed_Isuses.R;
import sa.app.advansed_Isuses.custom_views.MyEditText;
import sa.app.advansed_Isuses.custom_views.MyImageView;
import sa.app.advansed_Isuses.custom_views.MyTextView;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.Holder> {
    Context context;
    List<RegisterModel> models;

    public RecyclerAdapter(Context context, List<RegisterModel> models) {
        this.context = context;
        this.models = models;
    }


    @Override
    public RecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(context).inflate(R.layout.recycler_item, viewGroup, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter.Holder holder, int i) {
        holder.name.setText(models.get(i).name);
        Glide.with(context).load(models.get(i).urlImage).into(holder.image);
        holder.email.setText(models.get(i).email);
        holder.mobile.setText(models.get(i).mobile);

    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        MyTextView name, mobile, email;
        CircleImageView image;

        public Holder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            image = itemView.findViewById(R.id.img);
            mobile = itemView.findViewById(R.id.mobile);
            email = itemView.findViewById(R.id.email);

        }
    }
}
