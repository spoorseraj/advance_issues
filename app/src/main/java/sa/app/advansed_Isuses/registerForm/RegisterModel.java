package sa.app.advansed_Isuses.registerForm;

import com.orm.SugarRecord;

public class RegisterModel extends SugarRecord<RegisterModel>{
    String name,mobile,email,urlImage;


    public RegisterModel() {
        super();
    }

    public RegisterModel(String name, String mobile, String email, String urlImage) {
        super();
        this.name = name;
        this.mobile = mobile;
        this.email = email;
        this.urlImage = urlImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }
}
