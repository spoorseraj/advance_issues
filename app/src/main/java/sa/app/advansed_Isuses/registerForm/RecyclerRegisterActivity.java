package sa.app.advansed_Isuses.registerForm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import sa.app.advansed_Isuses.R;
import sa.app.advansed_Isuses.utiles.BaseActivity;

public class RecyclerRegisterActivity extends BaseActivity {
    RecyclerView recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_register);
        bind();
        loadData();
        findViewById(R.id.newBtn).setOnClickListener(v -> {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        });
    }

    private void loadData() {
        List<RegisterModel> rm = RegisterModel.listAll(RegisterModel.class);
        RecyclerAdapter recyclerAdapter = new RecyclerAdapter(mContext, rm);
        recycler.setAdapter(recyclerAdapter);
    }

    private void bind() {
        recycler = findViewById(R.id.recycler);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }
}
