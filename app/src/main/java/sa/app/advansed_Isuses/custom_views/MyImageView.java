package sa.app.advansed_Isuses.custom_views;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.bumptech.glide.Glide;

import sa.app.advansed_Isuses.utiles.BaseApplication;

public class MyImageView extends AppCompatImageView {
    Context mContext;
    public MyImageView(Context context) {
        super(context);
        mContext=context;
    }

    public MyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext=context;
    }
    public void load(String url){
        Glide.with(BaseApplication.baseApp).load(url).into(this);
    }
}
