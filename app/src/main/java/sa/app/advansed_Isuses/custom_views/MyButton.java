package sa.app.advansed_Isuses.custom_views;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import sa.app.advansed_Isuses.utiles.BaseApplication;

public class MyButton extends AppCompatButton {
    public MyButton(Context context) {
        super(context);
        this.setTypeface(BaseApplication.typeFace);
    }

    public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(BaseApplication.typeFace);
    }
}
