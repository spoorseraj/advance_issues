package sa.app.advansed_Isuses.custom_views;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.widget.EditText;

import sa.app.advansed_Isuses.utiles.BaseApplication;

public class MyEditText extends AppCompatEditText {
    public MyEditText(Context context) {
        super(context);
        this.setTypeface(BaseApplication.typeFace);
    }

    public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(BaseApplication.typeFace);
    }
    public String get(){
        return this.getText().toString();
    }
}
