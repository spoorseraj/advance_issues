package sa.app.advansed_Isuses.map;

import android.content.Intent;
import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Calendar;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import sa.app.advansed_Isuses.R;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    LatLng cLocation;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        findViewById(R.id.start).setOnClickListener(v -> {
            Intent intent = new Intent(this, LocationService.class);
            startService(intent);
        });
        findViewById(R.id.stop).setOnClickListener(v -> {
            Intent intent = new Intent(this, LocationService.class);
            stopService(intent);
        });

            }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        get_location();
        // Add a marker in Sydney and move the camera
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

//        if(timeOfDay <=8 && timeOfDay >= 0){
//            try {
//                mMap.setMapStyle(
//                        MapStyleOptions.loadRawResourceStyle(this, R.raw.darkmap));
//            } catch (Exception ex){
//
//            }
//        }else
            try {
                mMap.setMapStyle(
                        MapStyleOptions.loadRawResourceStyle(this, R.raw.map1));
            } catch (Exception ex){

            }




    }

    void get_location() {

    }


    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onLocation(Location location) {
        cLocation = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.addMarker(new MarkerOptions().position(cLocation).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(cLocation));

        CameraUpdate uLocation = CameraUpdateFactory.newLatLngZoom(cLocation, 16);
        mMap.animateCamera(uLocation);

    }

    @Subscribe
    public void onActivity(DetectedActivity detectedActivity) {
        Toast.makeText(this, detectedActivity.toString(), Toast.LENGTH_SHORT).show();
    }

}
