package sa.app.advansed_Isuses.map;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.location.DetectedActivity;


import org.greenrobot.eventbus.EventBus;

import io.nlopez.smartlocation.OnActivityUpdatedListener;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;


public class LocationService extends Service implements OnLocationUpdatedListener, OnActivityUpdatedListener {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "start", Toast.LENGTH_SHORT).show();
        SmartLocation.with(this).location().start(this);
        SmartLocation.with(this).activity().start(this);

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onLocationUpdated(Location location) {
        EventBus.getDefault().post(location);
    }

    @Override
    public void onActivityUpdated(DetectedActivity detectedActivity) {
        EventBus.getDefault().post(detectedActivity);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SmartLocation.with(this).location().stop();
        SmartLocation.with(this).activity().stop();
    }
}
