package sa.app.advansed_Isuses.utiles;

import android.app.Application;
import android.graphics.Typeface;

import com.orm.SugarApp;

import io.realm.Realm;

public class BaseApplication extends SugarApp {
    public static BaseApplication baseApp;
    public static Typeface typeFace;
    @Override
    public void onCreate() {
        super.onCreate();
        baseApp=this;
        typeFace=Typeface.createFromAsset(getAssets(),Constants.appFontName);
        Realm.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
