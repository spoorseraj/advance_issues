package sa.app.advansed_Isuses.realmMVP;

import java.util.List;

public class Presenter implements Contract.Presenter {
    Contract.Model model = new Model();
    Contract.View view;
    String mode="insert";
    int id=0;

    @Override
    public void attachView(Contract.View view) {
        this.view = view;
        model.attachPresenter(this);
        model.load();
    }

    @Override
    public void onStudentLoaded(List<StudentEntity> students) {
        view.onUserLoaded(students);
    }

    @Override
    public void register(String name, String family, String mobile, String age) {
        if (mode.equals("insert"))
            model.register(name, family, mobile, age);
        else if (mode.equals("update"))
            model.update(name, family, mobile, age,id);

    }

    @Override
    public void studentRegistered() {
        view.studentRegistered();
        model.load();
        mode="insert";
    }

    @Override
    public void onStudentDeleteConfirm(StudentEntity student) {
        view.studentConfirm(student);
    }

    @Override
    public void onStudentEdit(StudentEntity student) {
        mode="update";
        id=student.studentID;
        view.onStudentEdition(student);
    }

    @Override
    public void onStudentEdited() {
        mode="insert";
        id=0;
        view.studentRegistered();
        model.load();
    }


    @Override
    public void onStudentDelete(StudentEntity student) {
        model.studentDelete(student);

    }

    @Override
    public void onStudentMessage(String message) {
        view.studentMessage(message);
        model.load();
    }


}
