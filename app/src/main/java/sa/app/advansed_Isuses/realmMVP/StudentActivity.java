package sa.app.advansed_Isuses.realmMVP;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.List;

import sa.app.advansed_Isuses.R;

public class StudentActivity extends AppCompatActivity implements Contract.View,
StudentAdapter.SelectStudent{
    sa.app.advansed_Isuses.custom_views.MyEditText name, family, mobile, age;
    Contract.Presenter presenter = new Presenter();
    RecyclerView studentList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);
        bind();
        presenter.attachView(this);


    }

    private void bind() {
        name = findViewById(R.id.name);
        family = findViewById(R.id.family);
        mobile = findViewById(R.id.mobile);
        age = findViewById(R.id.age);
        studentList=findViewById(R.id.studentList);

        findViewById(R.id.save).setOnClickListener(v -> {
            presenter.register(name.get(),family.get(),mobile.get(),age.get());
        });

    }

    @Override
    public void onUserLoaded(List<StudentEntity> students) {
        StudentAdapter adapter=new StudentAdapter(this,students);
        studentList.setAdapter(adapter);
        adapter.selectStudent=this;
    }

    @Override
    public void studentRegistered() {
        name.setText("");
        family.setText("");
        mobile.setText("");
        age.setText("");
    }

    @Override
    public void studentConfirm(StudentEntity student) {
        AlertDialog.Builder alert=new AlertDialog.Builder(this);
        alert.setTitle("Delete student");
        alert.setMessage("are you sure you want to delete? ");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                presenter.onStudentDelete(student);
                dialogInterface.dismiss();
            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alert.show();
    }

    @Override
    public void studentMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStudentEdition(StudentEntity student) {
        name.setText(student.name);
        family.setText(student.family);
        mobile.setText(student.mobile);
        age.setText(student.age+"");
    }

    @Override
    public void onStudentEditSelect(StudentEntity student) {
        presenter.onStudentEdit(student);
    }

    @Override
    public void onStudentDeleteSelect(StudentEntity student) {
        presenter.onStudentDeleteConfirm(student);
    }
}
