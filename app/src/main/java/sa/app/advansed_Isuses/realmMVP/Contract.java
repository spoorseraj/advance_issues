package sa.app.advansed_Isuses.realmMVP;
import java.util.List;
public interface Contract {
    interface View{
        void onUserLoaded(List<StudentEntity> students);
        void studentRegistered();
        void studentConfirm(StudentEntity student);
        void studentMessage(String name);
        void onStudentEdition(StudentEntity student);
    }
    interface Presenter{
        void attachView(View view);
        void onStudentLoaded(List<StudentEntity> students);
        void register(String name,String Family,String mobile,String age);
        void studentRegistered();
        void onStudentDeleteConfirm(StudentEntity student);
        void onStudentEdit(StudentEntity student);
        void onStudentEdited();
        void onStudentDelete(StudentEntity student);
        void onStudentMessage(String message);
    }
    interface Model{
        void attachPresenter(Presenter presenter);
        void load();
        void register(String name,String Family,String mobile,String age);
        void update(String name,String Family,String mobile,String age,int id);
        void studentDelete(StudentEntity student);
    }
}
