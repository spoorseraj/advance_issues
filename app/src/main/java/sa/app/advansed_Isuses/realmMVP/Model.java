package sa.app.advansed_Isuses.realmMVP;

import io.realm.Realm;
import io.realm.RealmResults;

public class Model implements Contract.Model {
    Contract.Presenter presenter;
    Realm realm = Realm.getDefaultInstance();

    @Override
    public void attachPresenter(Contract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void load() {
        RealmResults<StudentEntity> students = realm.where(StudentEntity.class).findAll();
        presenter.onStudentLoaded(students);
    }

    @Override
    public void register(String name, String family, String mobile, String age) {
        if(!name.equals("")) {
            int newId = getNextKey();
            StudentEntity student = new StudentEntity(newId, name, family, mobile, "", Integer.parseInt(age));
            realm.beginTransaction();
            realm.copyToRealm(student);
            realm.commitTransaction();
            presenter.studentRegistered();
            presenter.onStudentMessage(name + " record is added");
        }
    }

    @Override
    public void update(String name, String family, String mobile, String age,int id) {
        realm.beginTransaction();
        StudentEntity std=realm.where(StudentEntity.class).equalTo("studentID",id).findFirst();
        if (std == null) {
//            StudentEntity s = realm.createObject(StudentEntity.class);
//            s.setName(name);
//            s.setFamily(family);
//            s.setMobile(mobile);
//            s.setAge(Integer.parseInt(age));

            // set the fields here
        } else {
            std.setName(name);
            std.setFamily(family);
            std.setMobile(mobile);
            std.setAge(Integer.parseInt(age));
            realm.insertOrUpdate(std);

        }
        realm.commitTransaction();
        presenter.onStudentEdited();
        presenter.onStudentMessage(name+" record is updated");
    }


    @Override
    public void studentDelete(StudentEntity student) {
        realm.beginTransaction();
        String name=student.name;
        RealmResults<StudentEntity> rows = realm.where(StudentEntity.class).
                equalTo("studentID",student.studentID).findAll();
        rows.deleteAllFromRealm();
        realm.commitTransaction();
        presenter.onStudentMessage(name+" record is deleted  from list!!");
    }

    private int getNextKey() {
        try {
            Number number =
                    realm.where(StudentEntity.class).max("studentID");

            if (number != null) {
                return number.intValue() + 1;
            } else {
                return 0;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            return 0;
        }
    }

}
