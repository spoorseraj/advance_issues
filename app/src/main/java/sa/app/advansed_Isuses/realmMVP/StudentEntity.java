package sa.app.advansed_Isuses.realmMVP;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class StudentEntity extends RealmObject {
    @PrimaryKey
    public int studentID;
    public String name,family,mobile,imageUrl;
    public int age;

    public StudentEntity() {
    }

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public StudentEntity(int studentID, String name, String family, String mobile, String imageUrl, int age) {

        this.studentID = studentID;
        this.name = name;
        this.family = family;
        this.mobile = mobile;
        this.imageUrl = imageUrl;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
