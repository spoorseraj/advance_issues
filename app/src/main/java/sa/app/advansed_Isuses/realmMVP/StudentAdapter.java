package sa.app.advansed_Isuses.realmMVP;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import sa.app.advansed_Isuses.R;
import sa.app.advansed_Isuses.custom_views.MyTextView;

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.Holder> {
    Context mContext;
    List<StudentEntity> students;

    public StudentAdapter(Context mContext, List<StudentEntity> students) {
        this.mContext = mContext;
        this.students = students;
    }

    @NonNull
    @Override
    public StudentAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.student_recycler_item, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StudentAdapter.Holder holder, int i) {
        holder.name.setText("name: "+students.get(i).name);
        holder.family.setText(students.get(i).family);
        holder.mobile.setText("mobile: "+students.get(i).mobile);
        holder.age.setText("age: "+students.get(i).age+"");

    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView name, family, mobile, age;
        CircleImageView image;
        ImageButton edit,delete;

        public Holder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            family = itemView.findViewById(R.id.family);
            mobile = itemView.findViewById(R.id.mobile);
            age = itemView.findViewById(R.id.age);
            image = itemView.findViewById(R.id.img);
            edit = itemView.findViewById(R.id.edit);
            delete = itemView.findViewById(R.id.delete);

            edit.setOnClickListener(v->{
                StudentEntity student=students.get(getAdapterPosition());
                selectStudent.onStudentEditSelect(student);
            });
            delete.setOnClickListener(v->{
                StudentEntity student=students.get(getAdapterPosition());
                selectStudent.onStudentDeleteSelect(student);
            });


        }
    }



    SelectStudent selectStudent;
    public void setSelectStudent(SelectStudent selectStudent) {
        this.selectStudent = selectStudent;
    }
    interface SelectStudent{
        void onStudentEditSelect(StudentEntity student);
        void onStudentDeleteSelect(StudentEntity student);
    }
}
